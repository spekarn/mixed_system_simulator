﻿# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
from math import e
from sympy import Symbol,  Integral

from PyQt5 import QtWidgets, QtGui
from PyQt5.QtWidgets import QApplication, QMainWindow, QMessageBox
import sys

#-------------FUNCTIONS----------------#

def cyclesDOD_VRLAB1(dod): #cycles(DOD)
    cycles=12850*e**(-9.738*dod)+3210*e**(-1.4299*dod)
    return cycles #

def cyclesDOD_IncellNMC(DOD): #cycles(DOD)
    cycles=151438 - 1.47529*10**6 * DOD + 7.41678*10**6 * DOD**2 - 2.14086*10**7 * DOD**3 + 3.68167*10**7 * DOD**4 - 3.7225*10**7 * DOD**5 + 2.04022*10**7 * DOD**6 - 4.67221*10**6 * DOD**7
    return cycles 

def SOCocv_14S_NMC(ocv): #OCV 40-60V (data from INR battery, tried making a function of ICR, but it was a tricky one) (INR better than a bad ICR-function) no time to fix
    SOC=-161681+16372.9*ocv-659.641*ocv**2+13.2154*ocv**3-0.131669*ocv**4 + 0.000522175*ocv**5 
    return SOC

def xpoint_V(liWhcap, laWhcap): #returns a Voltage point where LA and Li currents meet, based on tests at ratio .25 .5 1 at 3000W dsg
    ratio = liWhcap/laWhcap
    xpoint_V = -3.2323*ratio+51.632 
    return xpoint_V

def c_rateLA(avg_loadW, laWhcap): #returns a cap_factor, scaling LA capacity as a function of C-rate.
    c=avg_loadW/laWhcap
    cap_factor=-0.4953*c**4 + 0.5458*c**3 - 0.3016*c**2 - 0.2179*c + 1.0233
    return cap_factor

def R0_Wh(liWhcap): #baserat på 14S R0(Wh)
    r0_Wh = 86.688/liWhcap
    return r0_Wh

#def simulator(wh, w, h, ipy, st): #ipy=interruptions/year, st=simulation time

def simulator(wh, w, h, ipy, st, pli, pla):
    #-------------Data arrays for plotting----------------#
        
    arr_liWhcap = []
    arr_lila_ac = [] #lithium+lead annual cost
    arr_lila_cc = [] #lithium+lead cycle cost
    arr_costLi_bat = []
    arr_xp = []
    arr_dodLi = []
    arr_dodLA = []
    arr_li_ac = [] #lithium annual cost
    arr_la_ac = [] #lead annual cost
    arr_li_lifetime = []
    arr_la_lifetime = []
    arr_lila_roi = [] #lithium+lead return of investment (lithium)
    
    #-----------------USER INPUTS-------------------------#
    print("\n\nMixed system DOD simulator")
    print("------------------------------------------------------------------------------------")
    
    priceLi=pli#3 #kr/Wh #lithium 3x dyrare enligt Jesper B
    priceLA=pla#1 #kr/Wh
    
    #BatteryType = "NMC 14S"
    nom_voltage = 50.4
    avg_loadW = w #float(input("Input average Load W: "))  #load for calculations
    max_load_W = avg_loadW #check so that Li can handle max current
    #req_break_h = float(input("Input required interruption time h: "))       #required break,interuption time h, dimensioning factor for LA
    avg_break_h = h #float(input("Input average interruption time h: "))    #avg break,interuption time h, grid loss time
    avg_cycles_year = ipy #int(input("Input average cycles/year at avg load and avg int.time: "))  #avg cycles/year @ avg load and avg int. time
    max_load_W = avg_loadW #check so that Li can handle max current
    sim_time= st #float(input("Input desired simulation time, increase LiWhcap until x*LAWhcap: ")) #(sim_time*laWhcap) #limits simulation time if liWhcap >= sim_time*laWhcap:  break
    
    #---------Process data from USER INPUTS--------------#
    print("------------------------------------------------------------------------------------")
    Li_A_dsgmax = max_load_W/40 #40V minimum voltage
    print("\nLi A discharge max  : " + str(Li_A_dsgmax))
    
    #req_Wh = avg_loadW*req_break_h      #decides size of LA bat
    avg_Whdsg = avg_loadW*avg_break_h   #avg Whdsg
    
    liWhcap = nom_voltage*50 #starts low 50Ah rating and iterates
    
    laWhcap = wh #req_Wh*1.5 # normal overdim. *1.5 enligt Jesper B
    #LAAhcap=laWhcap/48  #LA nom voltage = 48V
    
    cap_factor=c_rateLA(avg_loadW, laWhcap) #see function description
    laWhcap_corr = laWhcap*cap_factor
    costLA_bat=priceLA*laWhcap #costLi_bat calculated in loop
    
    print("LA Wh cap   :" + str(round(laWhcap,2)))
    print("average Wh discharge  : " + str(round(avg_Whdsg,2)))
    
    #--------------Comparison calc-----------------------#
    
    costLA_alone_bat=priceLA*laWhcap #price 1kr/Wh LA
    costLi_alone_bat=priceLi*laWhcap
    
    dodLA_alone=avg_Whdsg/laWhcap
    cyclesLA_alone=cyclesDOD_VRLAB1(dodLA_alone)
    la_alonelifetime=cyclesLA_alone/avg_cycles_year
    la_alone_ac = costLA_alone_bat/la_alonelifetime
    print("\n--LA alone--" + 
          "\nlifetime  :" + str(round(la_alonelifetime,2)) + 
          "\nyearlycost :" + str(round(la_alone_ac,2)) +
          "\ncyclecost :" + str(round(costLA_alone_bat/cyclesLA_alone,2)))
    
    dodLi_alone=avg_Whdsg/laWhcap #dimensionerat efter orig. storlek LA
    cyclesLi_alone=cyclesDOD_IncellNMC(dodLi_alone)
    li_alonelifetime=cyclesLi_alone/avg_cycles_year
    li_alone_ac = costLi_alone_bat/li_alonelifetime
    print("\n--Li alone--"
          "\nlifetime :" + str(round(li_alonelifetime,2)) + 
          "\nyearlycost :" + str(round(li_alone_ac,2)) + 
          "\ncyclecost :" + str(round(costLi_alone_bat/cyclesLi_alone,2)))
    
    #-----------------------------------------------------#
    x = Symbol('x')        
    #func_s = -3*x**2 + 2*x**3 + 1   #0.40627/0.5 = 0.81254 #option, S-shaped waveform.
    func_x = 1-x #0.375/0.5 = 0.75
    
    #initial calc          #"values for next step, next graph shape/x-point"
    liWhcap_corr = (SOCocv_14S_NMC(54.5)/100)*liWhcap 
    totWh=laWhcap_corr+liWhcap_corr
    xpV = xpoint_V(liWhcap, laWhcap) # see function description #use non-corr li,la-capacity (function was based on it)
    current_mp=avg_loadW/xpV; volt_fall=(current_mp/2)*(R0_Wh(liWhcap))
    liWhdsgbefore_x=(SOCocv_14S_NMC(54.5)-SOCocv_14S_NMC(xpV+volt_fall))/100*liWhcap 
    whbefore_x=liWhdsgbefore_x/0.75 #0.81254
    laWhdsgbefore_x=whbefore_x-liWhdsgbefore_x
    whafter_x=totWh-whbefore_x
    
    while True:
        if liWhdsgbefore_x >= 0:
            break
        if liWhdsgbefore_x < 0:
            liWhcap = liWhcap*1.01
            xpV = xpoint_V(liWhcap, laWhcap) # see function description #use non-corr li,la-capacity (function was based on it)
            current_mp=avg_loadW/xpV; volt_fall=(current_mp/2)*(R0_Wh(liWhcap))
            liWhdsgbefore_x=(SOCocv_14S_NMC(54.5)-SOCocv_14S_NMC(xpV+volt_fall))/100*liWhcap # 
    
    
    #-----values for next step = next graph-shape with new x-point-----#
    liWhcap_corr = (SOCocv_14S_NMC(54.5)/100)*liWhcap #because NMC14S only use its capacity from 54.5V # charge voltage
    totWh=laWhcap_corr+liWhcap_corr
    xpV = xpoint_V(liWhcap, laWhcap) # see function description
    current_mp=avg_loadW/xpV; volt_fall=(current_mp/2)*(R0_Wh(liWhcap))
    liWhdsgbefore_x=(SOCocv_14S_NMC(54.5)-SOCocv_14S_NMC(xpV+volt_fall))/100*liWhcap # 
    whbefore_x=liWhdsgbefore_x/0.75 #0.81254
    laWhdsgbefore_x=whbefore_x-liWhdsgbefore_x
    whafter_x=totWh-whbefore_x    
    
    while True:
            arr_liWhcap.append(liWhcap)
            xp=(avg_Whdsg/whbefore_x)*0.5 #xpoint between 0-0.5  
            #arr_xp.append(xp)
            if xp >= 0.5:
                xp = 0.5 #if avg_Whdsg is past xpoint,
                # liWhdsgbefore_x and laWhdsgbefore_x remains unchanged            
            if xp < 0.5: #if avg_Whdsg is used before xpoint, calc integral
                liWhdsgbefore_x=float((Integral(func_x, (x, 0,xp)).doit())*whbefore_x*2)
                laWhdsgbefore_x=avg_Whdsg-liWhdsgbefore_x 
                          
            xpa = ((avg_Whdsg-whbefore_x)/whafter_x)*0.5+0.5 #xpoint after 0.5 remapped as 0.5-1
            if xpa <=0.5: #if avg_Whdsg is used before xpoint,
                liWhdsgafter_x = 0
                laWhdsgafter_x = 0
            if xpa >0.5:
                liwhafter_x = liWhcap_corr -liWhdsgbefore_x        
                liWhdsgafter_x=float((Integral(func_x, (x, 0.5, xpa)).doit())*liwhafter_x*8) #integral 0.5-1 is 0.125, see 0.125 as 100% liWhafter_x discharge, therefore x8
                laWhdsgafter_x = avg_Whdsg-whbefore_x-liWhdsgafter_x 
                
            li_Whdsg=liWhdsgbefore_x+liWhdsgafter_x
            la_Whdsg=laWhdsgbefore_x+laWhdsgafter_x  
            #print(li_Whdsg+la_Whdsg)
            dodLi=li_Whdsg/liWhcap; arr_dodLi.append(dodLi) 
            dodLA=la_Whdsg/laWhcap; arr_dodLA.append(dodLA) 
            cyclesLi=cyclesDOD_IncellNMC(dodLi)
            cyclesLA=cyclesDOD_VRLAB1(dodLA)
            
            li_lifetime=cyclesLi/avg_cycles_year; arr_li_lifetime.append(li_lifetime)  
            la_lifetime=cyclesLA/avg_cycles_year; arr_la_lifetime.append(la_lifetime) 
    
            #----economics----#        
            costLi_bat=priceLi*liWhcap #costLA_bat is constant, calc costLi_bat on non-corr LiWhcap
            li_ac = costLi_bat/li_lifetime; arr_li_ac.append(li_ac) 
            la_ac = costLA_bat/la_lifetime; arr_la_ac.append(la_ac)           
            lila_ac = li_ac + la_ac; arr_lila_ac.append(lila_ac) 
            lila_cc = costLi_bat/cyclesLi + costLA_bat/cyclesLA; arr_lila_cc.append(lila_cc)
            lila_roi = costLi_bat/(la_alone_ac-lila_ac); arr_lila_roi.append(lila_roi) #roi = return of investment. (Li battery)
            
            liWhcap = liWhcap*1.01 #increase Li with %
            #-----values for next step = next graph-shape with new x-point-----#
            liWhcap_corr = (SOCocv_14S_NMC(54.5)/100)*liWhcap #because NMC14S only use its capacity from 54.5V # charge voltage
            totWh=laWhcap_corr+liWhcap_corr
            xpV = xpoint_V(liWhcap, laWhcap) # see function description
            current_mp=avg_loadW/xpV; volt_fall=(current_mp/2)*(R0_Wh(liWhcap))
            liWhdsgbefore_x=(SOCocv_14S_NMC(54.5)-SOCocv_14S_NMC(xpV+volt_fall))/100*liWhcap # 
            whbefore_x=liWhdsgbefore_x/0.75 #0.81254
            laWhdsgbefore_x=whbefore_x-liWhdsgbefore_x
            whafter_x=totWh-whbefore_x    
            if liWhcap >= sim_time*laWhcap: 
                break
    
    data0 = arr_liWhcap
    data1 = arr_lila_ac
    data2 = arr_dodLA
    data3 = arr_dodLi
    data4 = arr_xp
    data5 = arr_li_ac
    data6 = arr_la_ac
    data7 = arr_li_lifetime
    data8 = arr_la_lifetime
    data9 = arr_lila_roi
    
    min_lila_ac=(min(arr_lila_ac))
    pos = arr_lila_ac.index(min_lila_ac)
    print("\n----LiLA----"
          "\nmin. LiLA yearlycost: " + str(round(min_lila_ac,2)) + " at liWhcap: " + str(round(arr_liWhcap[pos],2)) +
          "\ncostLi: " + str(round(((arr_liWhcap[pos])*priceLi),2)) + 
          "\nroi Li: " + str(round(((arr_liWhcap[pos])*priceLi)/(la_alone_ac-min_lila_ac),2)))            
    
    #---LA alone compens.--(LA added equal amount)---#
    newLaWhcap = laWhcap + (arr_liWhcap[pos]) # 
    newcostLA_alone_bat = priceLA*newLaWhcap
    newdodLA_alone=avg_Whdsg/newLaWhcap
    newcyclesLA_alone=cyclesDOD_VRLAB1(newdodLA_alone)
    newla_alonelifetime=newcyclesLA_alone/avg_cycles_year
    newla_alone_ac = newcostLA_alone_bat/newla_alonelifetime
    print("\n--LA alone compens.--(LA added equal amount: " + str(round(arr_liWhcap[pos],2)) + ")"
          "\nlifetime  :" + str(round(newla_alonelifetime,2)) + 
          "\nyearlycost :" + str(round(newla_alone_ac,2)) +
          "\ncyclecost :" + str(round((newcostLA_alone_bat/newcyclesLA_alone),2)))
    
    print("\n\n------------------------------------------------------------------------------------")
    print("version 1.0 \nmikael.claeson@incellint.com")
    #----------plot---------------------#
    
    fig3, (ax1, ax2) = plt.subplots(2)
    fig3.suptitle("Load W: " + str(avg_loadW) + ", time Whdsg: " + str(round(avg_Whdsg)) + ", LA Whcap: " + str(round(laWhcap,1)))
    # plt.title("Load W: " + str(avg_loadW) + ", time Whdsg: " + str(avg_Whdsg) + ", LA Whcap: " + str(laWhcap))
    
    ax1.grid()
    color = 'tab:blue'
    #ax1.set_xlabel('Li Whcap')
    ax1.set_ylabel('cost/year kr \n (cycles/year: ' + str(avg_cycles_year) +")" )  #, color=color
    ax1.plot(data0, data1,label='LiLA cost/year                          ',color=color )  #, color=color
    #ax1.tick_params(axis='y', labelcolor=color)
    ax1.legend()
    
    # ax1.set_xlabel('Li Whcap')
    # ax1.set_ylabel('cost/year kr    (cycles/year: ' + str(avg_cycles_year) +")" )  #, color=color
    # ax1.plot(data0, data4,label='xp                          ',color=color )  #, color=color
    # #ax1.tick_params(axis='y', labelcolor=color)
    # ax1.legend()
    
    ax = ax1.twinx()  # instantiate a second axes that shares the same x-axis
    color = 'tab:red'
    ax.set_ylabel('DOD') # , color=color # we already handled the x-label with ax1
    ax.plot(data0, data2, label='DOD LA', color=color )  #
    #ax2.tick_params(axis='y') #, labelcolor=color
    ax.legend()
    
    #ax = ax1.twinx()  # instantiate a second axes that shares the same x-axis
    color = 'tab:green'
    ax.set_ylabel('DOD') # , color=color # we already handled the x-label with ax1
    ax.plot(data0, data3, label='DOD Li', color=color )  #
    #ax2.tick_params(axis='y') #, labelcolor=color
    ax.legend()
    
    #fig3.tight_layout()
    
    color = 'tab:yellow'
    ax2.set_xlabel('liWhcap')
    ax2.set_ylabel('Lifetime \n (cycles/year: ' + str(avg_cycles_year) +")")  #, color=color
    ax2.plot(data0, data7,label='Li lifetime' )  #, color=color
    #ax2.tick_params(axis='y', labelcolor=color)
    ax2.legend()
    
    color = 'tab:green'
    ax2.set_xlabel('Li Whcap')
    #ax2.set_ylabel('Yearly cost')  #, color=color
    ax2.plot(data0, data8,label='LA lifetime' , color=color)  #
    #ax2.tick_params(axis='y', labelcolor=color)
    ax2.grid()
    ax2.legend()
     
    plt.show() 

       
#def simulator(wh, w, h, ipy, st): #ipy=interruptions/year, st=simulation time

def show():
    wh=float(lineWh.text())
    w=float(lineW.text())
    h=float(lineh.text())
    ipy=float(lineipy.text())
    st =float(combo.currentText()) #st = simulator time = st*laWhcap
    pli=float(linepli.text())
    pla=float(linepla.text())
    simulator(wh,w,h,ipy,st,pli,pla)
    
def helpsection():
    msg = QMessageBox(win)
    msg.setWindowTitle("Help")
    msg.setText("Only use values in Wh, W and hours.\n"
                "Take care not to input longer interruption time than size of LA.\n" 
                "LA Wh > W*interruption time h\n" 
                "Results are shown in 2 separate graphs including extra calculations in black screen.\n "
                "If lifetime of batteries exceeds their calender life, economic calculations are not relevant/true.\n"
                "For more in depth info of script see:\n"
                "http://www.diva-portal.org/smash/get/diva2:1450132/FULLTEXT01.pdf\n")
    x = msg.exec_()    
    
app = QApplication(sys.argv)
win = QMainWindow()
win.setGeometry(300,300,650,500)
win.setWindowTitle("Mixed System Simulator")


labelhead = QtWidgets.QLabel(win)
labelhead.setFont(QtGui.QFont("Times", 15, QtGui.QFont.Bold))
labelhead.setText("Mixed System Simulator")
labelhead.adjustSize()
labelhead.move(30,20)

labelWh = QtWidgets.QLabel(win)
labelWh.setFont(QtGui.QFont("Times", 12, QtGui.QFont.Bold))
labelWh.setText("LA capacity Wh:")
labelWh.adjustSize()
labelWh.move(30,80)
lineWh = QtWidgets.QLineEdit(win)
lineWh.move(500,80)

labelW = QtWidgets.QLabel(win)
labelW.setFont(QtGui.QFont("Times", 12, QtGui.QFont.Bold))
labelW.setText("Load W:")
labelW.adjustSize()
labelW.move(30,120)
lineW = QtWidgets.QLineEdit(win)
lineW.move(500,120)

labelh = QtWidgets.QLabel(win)
labelh.setFont(QtGui.QFont("Times", 12, QtGui.QFont.Bold))
labelh.setText("Interruption time h:")
labelh.adjustSize()
labelh.move(30,160)
lineh = QtWidgets.QLineEdit(win)
lineh.move(500,160)

labelipy = QtWidgets.QLabel(win)
labelipy.setFont(QtGui.QFont("Times", 12, QtGui.QFont.Bold))
labelipy.setText("Interruptions/year:")
labelipy.adjustSize()
labelipy.move(30,200)
lineipy = QtWidgets.QLineEdit(win)
lineipy.move(500,200)

labelipy = QtWidgets.QLabel(win)
labelipy.setFont(QtGui.QFont("Times", 12, QtGui.QFont.Bold))
labelipy.setText("Max Li-Ion Wh (x*LA Wh cap):")
labelipy.adjustSize()
labelipy.move(30,240) 
combo = QtWidgets.QComboBox(win)
combo.addItems(["0.10", "0.15","0.166", "0.2","0.222","0.25", "0.5", "0.75", "1.0", "1.25", "1.5", "1.75", "2.0", "2.5", "3.0" ])
combo.move(500,240)
 
labelpli = QtWidgets.QLabel(win)
labelpli.setFont(QtGui.QFont("Times", 12, QtGui.QFont.Bold))
labelpli.setText("Price Li/Wh kr:")
labelpli.adjustSize()
labelpli.move(30,280)
linepli = QtWidgets.QLineEdit(win)
linepli.move(200,280)

labelpla = QtWidgets.QLabel(win)
labelpla.setFont(QtGui.QFont("Times", 12, QtGui.QFont.Bold))
labelpla.setText("Price LA/Wh kr:")
labelpla.adjustSize()
labelpla.move(30,320)
linepla = QtWidgets.QLineEdit(win)
linepla.move(200,320)


button = QtWidgets.QPushButton(win)
button.setText("Submit")
button.clicked.connect(show)
button.move(500,400)
 
button = QtWidgets.QPushButton(win)
button.setText("Clear")
button.clicked.connect(lineWh.clear)
button.clicked.connect(lineW.clear)
button.clicked.connect(lineh.clear)
button.clicked.connect(lineipy.clear)
button.move(500,440)

button = QtWidgets.QPushButton(win)
button.setText("Help")
button.clicked.connect(helpsection)
button.move(500,30)


 

 
win.show()
sys.exit(app.exec_())


