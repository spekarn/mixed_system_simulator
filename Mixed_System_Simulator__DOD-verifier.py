﻿# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
from math import e
from sympy import Symbol,  Integral

from PyQt5 import QtWidgets, QtGui
from PyQt5.QtWidgets import QApplication, QMainWindow, QMessageBox
import sys

import xlsxwriter


#-------------FUNCTIONS----------------#

def cyclesDOD_VRLAB1(dod): #cycles(DOD)
    cycles=12850*e**(-9.738*dod)+3210*e**(-1.4299*dod)
    return cycles #

def cyclesDOD_IncellNMC(DOD): #cycles(DOD)
    cycles=151438 - 1.47529*10**6 * DOD + 7.41678*10**6 * DOD**2 - 2.14086*10**7 * DOD**3 + 3.68167*10**7 * DOD**4 - 3.7225*10**7 * DOD**5 + 2.04022*10**7 * DOD**6 - 4.67221*10**6 * DOD**7
    return cycles 

def SOCocv_14S_NMC(ocv): #OCV 40-60V (data from INR battery, tried making a function of ICR, but it was a tricky one) (INR better than a bad ICR-function) no time to fix
    SOC=-161681+16372.9*ocv-659.641*ocv**2+13.2154*ocv**3-0.131669*ocv**4 + 0.000522175*ocv**5 
    return SOC

def xpoint_V(liWhcap, laWhcap): #returns a Voltage point where LA and Li currents meet, based on tests at ratio .25 .5 1 at 3000W dsg
    ratio = liWhcap/laWhcap
    xpoint_V = -3.2323*ratio+51.632 
    return xpoint_V

def c_rateLA(avg_loadW, laWhcap): #returns a cap_factor, scaling LA capacity as a function of C-rate.
    c=avg_loadW/laWhcap
    cap_factor=-0.4953*c**4 + 0.5458*c**3 - 0.3016*c**2 - 0.2179*c + 1.0233
    return cap_factor

def R0_Wh(liWhcap): #baserat på 14S R0(Wh)
    r0_Wh = 86.688/liWhcap
    return r0_Wh

#def simulator(w, whla, whli, st): st=simulation time

def simulator(w, whla, whli, st):
    #-------------Data arrays for plotting----------------#
        
    arr_liWhcap = []
    arr_lila_ac = [] #lithium+lead annual cost
    arr_lila_cc = [] #lithium+lead cycle cost
    arr_xp = []
    arr_dodLi = []
    arr_dodLA = []
    arr_avg_Whdsg = []
    
    #-----------------USER INPUTS-------------------------#
    print("\n\nMixed system DOD simulator__DOD-verifier")
    print("------------------------------------------------------------------------------------")
    
    #BatteryType = "NMC 14S"
    nom_voltage = 50.4
    avg_loadW = w 
    sim_time = st
    
    #---------Process data from USER INPUTS--------------#
    print("------------------------------------------------------------------------------------")
    
    avg_Whdsg = 10 # starts at 10W and iterates 10*0.01    
    liWhcap = whli    
    laWhcap = whla    
    cap_factor=c_rateLA(avg_loadW, laWhcap) #see function description
    laWhcap_corr = laWhcap*cap_factor
    
    print("LA Wh cap   :" + str(round(laWhcap,2)))
    print("Li Wh cap   :" + str(round(liWhcap,2)))
    

    #-----------------------------------------------------#
    x = Symbol('x')        
    #func_s = -3*x**2 + 2*x**3 + 1   #0.40627/0.5 = 0.81254 #option, S-shaped waveform.
    func_x = 1-x #0.375/0.5 = 0.75
    
    #initial calc          #"values for next step, next graph shape/x-point"
    liWhcap_corr = (SOCocv_14S_NMC(54.5)/100)*liWhcap 
    totWh=laWhcap_corr+liWhcap_corr
    xpV = xpoint_V(liWhcap, laWhcap) # see function description #use non-corr li,la-capacity (function was based on it)
    current_mp=avg_loadW/xpV; volt_fall=(current_mp/2)*(R0_Wh(liWhcap))
    liWhdsgbefore_x=(SOCocv_14S_NMC(54.5)-SOCocv_14S_NMC(xpV+volt_fall))/100*liWhcap 
    whbefore_x=liWhdsgbefore_x/0.75 #0.81254
    laWhdsgbefore_x=whbefore_x-liWhdsgbefore_x
    whafter_x=totWh-whbefore_x
    
    while True:
        if liWhdsgbefore_x >= 0:
            break
        if liWhdsgbefore_x < 0:
            liWhcap = liWhcap*1.01
            xpV = xpoint_V(liWhcap, laWhcap) # see function description #use non-corr li,la-capacity (function was based on it)
            current_mp=avg_loadW/xpV; volt_fall=(current_mp/2)*(R0_Wh(liWhcap))
            liWhdsgbefore_x=(SOCocv_14S_NMC(54.5)-SOCocv_14S_NMC(xpV+volt_fall))/100*liWhcap # 
    
    
    #-----values for next step = next graph-shape with new x-point-----#
    liWhcap_corr = (SOCocv_14S_NMC(54.5)/100)*liWhcap #because NMC14S only use its capacity from 54.5V # charge voltage
    totWh=laWhcap_corr+liWhcap_corr
    xpV = xpoint_V(liWhcap, laWhcap) # see function description
    current_mp=avg_loadW/xpV; volt_fall=(current_mp/2)*(R0_Wh(liWhcap))
    liWhdsgbefore_x=(SOCocv_14S_NMC(54.5)-SOCocv_14S_NMC(xpV+volt_fall))/100*liWhcap # 
    whbefore_x=liWhdsgbefore_x/0.75 #0.81254
    laWhdsgbefore_x=whbefore_x-liWhdsgbefore_x
    whafter_x=totWh-whbefore_x    
    
    while True:
            arr_liWhcap.append(liWhcap)
            arr_avg_Whdsg.append(avg_Whdsg)            
            xp=(avg_Whdsg/whbefore_x)*0.5 #xpoint between 0-0.5  
            #arr_xp.append(xp)
            if xp >= 0.5:
                xp = 0.5 #if avg_Whdsg is past xpoint,
                # liWhdsgbefore_x and laWhdsgbefore_x remains unchanged            
            if xp < 0.5: #if avg_Whdsg is used before xpoint, calc integral
                liWhdsgbefore_x=float((Integral(func_x, (x, 0,xp)).doit())*whbefore_x*2)
                laWhdsgbefore_x=avg_Whdsg-liWhdsgbefore_x 
                          
            xpa = ((avg_Whdsg-whbefore_x)/whafter_x)*0.5+0.5 #xpoint after 0.5 remapped as 0.5-1
            if xpa <=0.5: #if avg_Whdsg is used before xpoint,
                liWhdsgafter_x = 0
                laWhdsgafter_x = 0
            if xpa >0.5:
                liwhafter_x = liWhcap_corr -liWhdsgbefore_x        
                liWhdsgafter_x=float((Integral(func_x, (x, 0.5, xpa)).doit())*liwhafter_x*8) #integral 0.5-1 is 0.125, see 0.125 as 100% liWhafter_x discharge, therefore x8
                laWhdsgafter_x = avg_Whdsg-whbefore_x-liWhdsgafter_x 
                
            li_Whdsg=liWhdsgbefore_x+liWhdsgafter_x
            la_Whdsg=laWhdsgbefore_x+laWhdsgafter_x  
            #print(li_Whdsg+la_Whdsg)
            dodLi=(li_Whdsg/liWhcap)*100; arr_dodLi.append(dodLi) 
            dodLA=(la_Whdsg/laWhcap)*100; arr_dodLA.append(dodLA) 

            #liWhcap = liWhcap*1.01 #increase Li with %
            avg_Whdsg = avg_Whdsg*1.01
            
            #-----values for next step = next graph-shape with new x-point-----#
            liWhcap_corr = (SOCocv_14S_NMC(54.5)/100)*liWhcap #because NMC14S only use its capacity from 54.5V # charge voltage
            totWh=laWhcap_corr+liWhcap_corr
            xpV = xpoint_V(liWhcap, laWhcap) # see function description
            current_mp=avg_loadW/xpV; volt_fall=(current_mp/2)*(R0_Wh(liWhcap))
            liWhdsgbefore_x=(SOCocv_14S_NMC(54.5)-SOCocv_14S_NMC(xpV+volt_fall))/100*liWhcap # 
            whbefore_x=liWhdsgbefore_x/0.75 #0.81254
            laWhdsgbefore_x=whbefore_x-liWhdsgbefore_x
            whafter_x=totWh-whbefore_x    
            # if liWhcap >= sim_time*laWhcap: 
            #     break
            if avg_Whdsg >= sim_time*laWhcap: 
                break 
            
    data0 = arr_liWhcap
    data1 = arr_lila_ac
    data2 = arr_dodLA
    data3 = arr_dodLi
    data4 = arr_xp
    data10 = arr_avg_Whdsg

    print("\n\n------------------------------------------------------------------------------------")
    print("version 1.0 \nmikael.claeson@incellint.com")
    #----------plot---------------------#
    
  #  fig3, (ax1, ax2) = plt.subplots(2)
    fig3, (ax) = plt.subplots(1)    
    fig3.suptitle("Load W: " + str(avg_loadW) + ", Li Whcap: " + str(round(liWhcap)) + ", LA Whcap: " + str(round(laWhcap,1)))
    # plt.title("Load W: " + str(avg_loadW) + ", time Whdsg: " + str(avg_Whdsg) + ", LA Whcap: " + str(laWhcap))
    
    ax.grid()

    #ax = ax1.twinx()  # instantiate a second axes that shares the same x-axis
    color = 'tab:red'
    ax.set_ylabel('DOD') # , color=color # we already handled the x-label with ax1
    ax.plot(data10, data2, label='DOD LA', color=color )  #
    #ax2.tick_params(axis='y') #, labelcolor=color
    ax.legend()
    
    #ax = ax1.twinx()  # instantiate a second axes that shares the same x-axis
    color = 'tab:green'
    ax.set_ylabel('DOD') # , color=color # we already handled the x-label with ax1
    ax.plot(data10, data3, label='DOD Li', color=color )  #
    #ax2.tick_params(axis='y') #, labelcolor=color
    ax.legend()
         
    plt.show() 
    
    workbook = xlsxwriter.Workbook('DOD_verifier_' + str(w) + 'W_' + str(whli) + 'WhLi_' + str(whla) + 'WhLA.xlsx', {'strings_to_numbers': True}) 
    worksheet1 = workbook.add_worksheet("Data") #original log_modbus.csv to xlsx
    
    for i in range(1,len(arr_avg_Whdsg),1):
        worksheet1.write(i,0, arr_avg_Whdsg[i]) #writing data to sheet1  
    for i in range(1,len(data2),1):
        worksheet1.write(i,1, data2[i]) #writing data to sheet1 
    for i in range(1,len(data3),1):
        worksheet1.write(i,2, data3[i]) #writing data to sheet1 
    workbook.close()     

#def simulator(w, whla, whli, st): st=simulation time

def show():
    w=float(lineW.text())
    whla=float(lineWhla.text())
    whli=float(lineWhli.text())
    st =float(combo.currentText()) #st = simulator time = st*laWhcap
    simulator(w,whla,whli,st)
    

    
app = QApplication(sys.argv)
win = QMainWindow()
win.setGeometry(300,300,650,500)
win.setWindowTitle("Mixed System Simulator__DOD-verifier")


labelhead = QtWidgets.QLabel(win)
labelhead.setFont(QtGui.QFont("Times", 15, QtGui.QFont.Bold))
labelhead.setText("Mixed System Simulator")
labelhead.adjustSize()
labelhead.move(30,20)

labelWhla = QtWidgets.QLabel(win)
labelWhla.setFont(QtGui.QFont("Times", 12, QtGui.QFont.Bold))
labelWhla.setText("LA capacity Wh:")
labelWhla.adjustSize()
labelWhla.move(30,80)
lineWhla = QtWidgets.QLineEdit(win)
lineWhla.move(500,80)

labelW = QtWidgets.QLabel(win)
labelW.setFont(QtGui.QFont("Times", 12, QtGui.QFont.Bold))
labelW.setText("Load W:")
labelW.adjustSize()
labelW.move(30,120)
lineW = QtWidgets.QLineEdit(win)
lineW.move(500,120)

labelWhli = QtWidgets.QLabel(win)
labelWhli.setFont(QtGui.QFont("Times", 12, QtGui.QFont.Bold))
labelWhli.setText("Li capacity Wh:")
labelWhli.adjustSize()
labelWhli.move(30,160)
lineWhli = QtWidgets.QLineEdit(win)
lineWhli.move(500,160)

labelipy = QtWidgets.QLabel(win)
labelipy.setFont(QtGui.QFont("Times", 12, QtGui.QFont.Bold))
labelipy.setText("Simulation lenght Whdsg = (x*LA Wh cap):")
labelipy.adjustSize()
labelipy.move(30,240) 
combo = QtWidgets.QComboBox(win)
combo.addItems(["0.25", "0.5", "0.75", "1.0", "1.25", "1.5", "1.75", "2.0", "2.5", "3.0" ])
combo.move(500,240)
 

button = QtWidgets.QPushButton(win)
button.setText("Submit")
button.clicked.connect(show)
button.move(500,400)
 
button = QtWidgets.QPushButton(win)
button.setText("Clear")
button.clicked.connect(lineW.clear)
button.clicked.connect(lineWhla.clear)
button.clicked.connect(lineWhli.clear)
button.move(500,440)

 
win.show()
sys.exit(app.exec_())


